from models.customers import database
data = {
    "params":[
            {
                "values" : {
                    "username" : "userpertama",
                    "namadepan" : "rudi pertes",
                    "namabelakang" : "pertest",
                    "email" : "rudi.pertest@gmail.com"
                
                }
            },
            {
                "values" :{
                    "username" : "userkeduda",
                    "namadepan" : "shiroe",
                    "namabelakang" : "ishigami",
                    "email" : "shiroee@gmail.com"
                }
            },
            {
                "values" :{
                    "username" : "userketiga",
                    "namadepan" : "akatsuki",
                    "namabelakang" : "horizon",
                    "email" : "akatsuki.horizon@gmail.com"
                }
            }
        ]
}




def ubahData(data):
    for param in data['params']:
        mysqldb.updateUserById(**param)
    mysqldb.dataCommit()
    print("data berhasil diubah")



if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print("Connected to MySQL database")
    
    ubahData(data)
    

    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()